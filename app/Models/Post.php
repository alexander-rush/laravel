<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
    	'title', 'text', 'author_id'
    ];

    public function author() {
    	$this->belongsTo('App\Models\User');
    }
}
