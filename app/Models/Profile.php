<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
    	'user_id', 'full_name', 'birthdate', 'address', 'phone', 'email'
    ];

    public function user() {
    	return $this->belongsTo('App\Models\User');
    }
}
