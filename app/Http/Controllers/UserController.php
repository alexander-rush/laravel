<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() {
        if (\Auth::user()->isStudent()) {
            return view('student', ['students' => User::all()->where('role_id', \Auth::user()->role_id)]);
        }
        if (\Auth::user()->isTeacher()) {
            return view('teacher', ['teachers' => User::all()->where('role_id', \Auth::user()->role_id)]);
        }
        if (\Auth::user()->isStaff()) {
            return view('staff', ['users' => User::all()]);
        }
    }
}
