<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
	$users = collect(App\Models\User::all());

    return [
        'title' => $faker->sentence(),
        'text' => $faker->realText(100),
        'author_id' => $users->random()->id
    ];
});
