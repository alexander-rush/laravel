<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'user_id' => function () {
        	return factory(App\Models\User::class)->create()->id;
        },
        'full_name' => function (array $user) {
        	return App\Models\User::find($user['user_id'])->name;
        },
        'birthdate' => $faker->date(),
        'address' => $faker->address(),
        'phone' => $faker->unique()->tollFreePhoneNumber(),
        'email' => function (Array $user) {
        	return App\Models\User::find($user['user_id'])->email;
        }
    ];
});
