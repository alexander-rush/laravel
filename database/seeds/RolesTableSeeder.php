<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'name' => 'Student',
        	'slug' => 'student'
        ]);

        Role::create([
        	'name' => 'Teacher',
        	'slug' => 'teacher'
        ]);

        Role::create([
        	'name' => 'Staff',
        	'slug' => 'staff'
        ]);
    }
}
