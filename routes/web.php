<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('student')->middleware('student')->group(function() {
	Route::get('/', 'UserController@index');
});

Route::prefix('teacher')->middleware('teacher')->group(function() {
	Route::get('/', 'UserController@index');
});

Route::prefix('staff')->middleware('staff')->group(function() {
	Route::get('/', 'UserController@index');
});

Route::namespace('Support')->middleware(['staff'])->group(function() {
	route::get('/support', 'SupportController@index')->name('support');
});

