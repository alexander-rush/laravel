@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				@foreach($teachers as $teacher)
					<div class="card">
						<div class="card-header">{{ $teacher->name }} <b>({{ $teacher->role->name }})</b></div>
						<div class="card-body">
							<p><b>email: </b>{{ $teacher->email }}</p>
							<p><b>created at: </b>{{ $teacher->created_at }}</p>
						</div>
					</div>
					<br>
				@endforeach
			</div>
		</div>
	</div>

@endsection