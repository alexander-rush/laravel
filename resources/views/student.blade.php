@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				@foreach($students as $student)
					<div class="card">
						<div class="card-header">{{ $student->name }} <b>({{ $student->role->name }})</b></div>
						<div class="card-body">
							<p><b>email: </b>{{ $student->email }}</p>
							<p><b>created at: </b>{{ $student->created_at }}</p>
						</div>
					</div>
					<br>
				@endforeach
			</div>
		</div>
	</div>

@endsection