@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				@foreach($users as $user)
					<div class="card">
						<div class="card-header">{{ $user->name }} <b>({{ $user->role->name }})</b></div>
						<div class="card-body">
							<p><b>email: </b>{{ $user->email }}</p>
							<p><b>created at: </b>{{ $user->created_at }}</p>
						</div>
					</div>
					<br>
				@endforeach
			</div>
		</div>
	</div>

@endsection