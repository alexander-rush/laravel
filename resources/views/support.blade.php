@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				@foreach ($profiles as $profile)
					<div class="card">
						<div class="card-header">
							{{ $profile->full_name }}
						</div>
						<div class="card-body">
							<p><b>email: </b>{{ $profile->email }}</p>
							<p><b>birthdate: </b>{{ $profile->birthdate }}</p>
							<p><b>address: </b>{{ $profile->address }}</p>
							<p><b>phone: </b>{{ $profile->phone }}</p>
						</div>
					</div>
					<br>
				@endforeach
			</div>
		</div>
	</div>

@endsection